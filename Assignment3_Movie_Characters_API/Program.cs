using Assignment3_Movie_Characters_API.Models;
using Assignment3_Movie_Characters_API.Services.Character;
using Assignment3_Movie_Characters_API.Services.Characters;
using Assignment3_Movie_Franchises_API.Services.Franchise;
using Assignment3_Movie_Movies_API.Services.Movie;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace Assignment3_Movie_Characters_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);


            // Add services to the container.

            builder.Services.AddDbContext<MovieDbContext>(
                    opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Movie Characters API",
                    Description = "API for movie characters",
                    Contact = new OpenApiContact
                    {
                        Name = "Simon Aa. Sutterud",
                        Url = new Uri("https://www.linkedin.com/in/simon-aannestad-sutterud/")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT 2022",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                options.IncludeXmlComments(xmlPath);
            }
            );

            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            builder.Services.AddTransient<ICharacterService, CharacterServiceImpl>();
            builder.Services.AddTransient<IFranchiseService, FranchiseServiceImpl>();
            builder.Services.AddTransient<IMovieService, MovieServiceImpl>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}