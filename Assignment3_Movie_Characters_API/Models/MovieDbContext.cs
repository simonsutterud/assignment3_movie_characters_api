﻿using Assignment3_Movie_Characters_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Assignment3_Movie_Characters_API.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; } = null!;
        public DbSet<Franchise> Franchises { get; set; } = null!;
        public DbSet<Movie> Movies { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, Name = "Dwayne Johnson", Alias = "The Rock", Gender = "Male", Picture = "www" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, Name = "Dlayne Thomson", Alias = "The Tock", Gender = "Male", Picture = "www" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, Name = "Payne Pohnson", Alias = "The Pock", Gender = "Male", Picture = "www" });

            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "My Franchise", Description = "The best franchise!" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 2, Name = "My other Franchise", Description = "The second best franchise!" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 3, Name = "My third Franchise", Description = "The third best franchise!" });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, Title = "Inception", Director = "Tarantino", Genre = "Action", Picture = "www", ReleaseYear = 2010, Trailer = "youtube.com", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, Title = "Avatar", Director = "Some guy", Genre = "Sci-fi", Picture = "www", ReleaseYear = 2010, FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, Title = "Harry Potter", Director = "Not JK Rowling", Genre = "Action", Picture = "www", ReleaseYear = 2010, Trailer = "youtube.com", FranchiseId = 1 });


            // Seed m2m coach-certification. Need to define m2m and access linking table
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    Character => Character.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    Movie => Movie.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 1 }
                        );
                    });
        }


    }
}
