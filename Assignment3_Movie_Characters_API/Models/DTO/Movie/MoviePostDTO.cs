﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Movie
{
    public class MoviePostDTO
    {
        public string Title { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        public string Director { get; set; } = null!;
        public string Picture { get; set; } = null!;
    }
}
