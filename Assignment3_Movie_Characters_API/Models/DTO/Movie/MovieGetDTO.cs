﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Movie
{
    public class MovieGetDTO
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        public string Director { get; set; } = null!;
        public string Picture { get; set; } = null!;
        public string? Trailer { get; set; }
        public int? FranchiseId { get; set; }
    }
}
