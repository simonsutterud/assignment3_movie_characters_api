﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Character
{
    public class CharacterPutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Alias { get; set; }
        public string Gender { get; set; } = null!;
        public string Picture { get; set; } = null!;
    }
}
