﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Character
{
    public class CharacterPostDTO
    {
        public string Name { get; set; } = null!;
        public string Gender { get; set; } = null!;
        public string Picture { get; set; } = null!;
    }
}
