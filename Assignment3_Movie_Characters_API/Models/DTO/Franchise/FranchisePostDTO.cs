﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Franchise
{
    public class FranchisePostDTO
    {
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
    }
}
