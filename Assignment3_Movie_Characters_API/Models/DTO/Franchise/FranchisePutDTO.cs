﻿namespace Assignment3_Movie_Characters_API.Models.DTO.Franchise
{
    public class FranchisePutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
    }
}
