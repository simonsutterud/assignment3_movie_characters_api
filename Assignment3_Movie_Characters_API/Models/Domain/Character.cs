﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3_Movie_Characters_API.Models.Domain
{
    [Table("Character")]
    public class Character
    {
        // PK
        public int Id { get; set; }

        // Fields
        [Required]
        [MaxLength(80)]
        public string Name { get; set; } = null!;
        public string? Alias { get; set; }
        [Required]
        [MaxLength(10)]
        public string Gender { get; set; } = null!;
        [Required]
        public string Picture { get; set; } = null!;

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
