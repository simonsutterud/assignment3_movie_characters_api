﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3_Movie_Characters_API.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        // PK
        public int Id { get; set; }

        // Fields
        public string Title { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        public string Director { get; set; } = null!;
        public string Picture { get; set; } = null!;
        public string? Trailer { get; set; }

        // Relationships
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }

    }
}
