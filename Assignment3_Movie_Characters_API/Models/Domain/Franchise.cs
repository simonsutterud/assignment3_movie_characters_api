﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3_Movie_Characters_API.Models.Domain
{
    [Table("Franchise")]
    public class Franchise
    {
        // PK
        public int Id { get; set; }

        // Fields
        [Required]
        [MaxLength(100)]

        public string Name { get; set; } = null!;
        [Required]
        public string Description { get; set; } = null!;

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
