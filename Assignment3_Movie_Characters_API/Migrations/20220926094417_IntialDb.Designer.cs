﻿// <auto-generated />
using System;
using Assignment3_Movie_Characters_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Assignment3_Movie_Characters_API.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    [Migration("20220926094417_IntialDb")]
    partial class IntialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("Assignment3_Movie_Characters_API.Models.Domain.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Alias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Gender")
                        .IsRequired()
                        .HasMaxLength(10)
                        .HasColumnType("nvarchar(10)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(80)
                        .HasColumnType("nvarchar(80)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Character");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "The Rock",
                            Gender = "Male",
                            Name = "Dwayne Johnson",
                            Picture = "www"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "The Tock",
                            Gender = "Male",
                            Name = "Dlayne Thomson",
                            Picture = "www"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "The Pock",
                            Gender = "Male",
                            Name = "Payne Pohnson",
                            Picture = "www"
                        });
                });

            modelBuilder.Entity("Assignment3_Movie_Characters_API.Models.Domain.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "The best franchise!",
                            Name = "My Franchise"
                        },
                        new
                        {
                            Id = 2,
                            Description = "The second best franchise!",
                            Name = "My other Franchise"
                        },
                        new
                        {
                            Id = 3,
                            Description = "The third best franchise!",
                            Name = "My third Franchise"
                        });
                });

            modelBuilder.Entity("Assignment3_Movie_Characters_API.Models.Domain.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ReleaseYear")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Trailer")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Tarantino",
                            FranchiseId = 1,
                            Genre = "Action",
                            Picture = "www",
                            ReleaseYear = 2010,
                            Title = "Inception",
                            Trailer = "youtube.com"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Some guy",
                            FranchiseId = 2,
                            Genre = "Sci-fi",
                            Picture = "www",
                            ReleaseYear = 2010,
                            Title = "Avatar"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Not JK Rowling",
                            FranchiseId = 1,
                            Genre = "Action",
                            Picture = "www",
                            ReleaseYear = 2010,
                            Title = "Harry Potter",
                            Trailer = "youtube.com"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.HasKey("CharacterId", "MovieId");

                    b.HasIndex("MovieId");

                    b.ToTable("CharacterMovie");

                    b.HasData(
                        new
                        {
                            CharacterId = 1,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 1,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 2,
                            MovieId = 1
                        });
                });

            modelBuilder.Entity("Assignment3_Movie_Characters_API.Models.Domain.Movie", b =>
                {
                    b.HasOne("Assignment3_Movie_Characters_API.Models.Domain.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId");

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("Assignment3_Movie_Characters_API.Models.Domain.Character", null)
                        .WithMany()
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Assignment3_Movie_Characters_API.Models.Domain.Movie", null)
                        .WithMany()
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Assignment3_Movie_Characters_API.Models.Domain.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
