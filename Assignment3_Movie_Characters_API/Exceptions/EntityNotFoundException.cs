﻿namespace Assignment3_Movie_Characters_API.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string? message) : base(message)
        {
        }

    }
}
