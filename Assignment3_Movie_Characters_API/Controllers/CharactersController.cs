﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Models.DTO.Character;
using Assignment3_Movie_Characters_API.Services.Characters;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Assignment3_Movie_Characters_API.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Gets all the characters in the database with their related movies represented as Ids.
        /// </summary>
        /// <returns>List of CharacterGetDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharacters()
        {
            return Ok(
                _mapper.Map<List<CharacterGetDTO>>(
                    await _characterService.GetAllAsync())
                );
        }

        /// <summary>
        /// Gets a single character with the specified Id and maps to CharacterGetDTO
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Single CharacterGetDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterGetDTO>> GetCharacter(int id)
        {
            try
            {
                return Ok(_mapper.Map<CharacterGetDTO>(
                        await _characterService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates an existing character. (Just values specific to the character entity.
        /// Other, related data can be updated in separate endpoints.)
        /// Returns a failed state if the Ids dont match or the character does not exist.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacterAsync(int id, CharacterPutDTO character)
        {
            if (id != character.Id)
                return BadRequest();

            try
            {
                await _characterService.UpdateAsync(
                       _mapper.Map<Character>(character)
                   );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Adds a new character to the database.
        /// </summary>
        /// <param name="characterDto"></param>
        [HttpPost]
        public async Task<IActionResult> PostCharacterAsync(CharacterPostDTO characterDto)
        {
            Character character = _mapper.Map<Character>(characterDto);
            await _characterService.AddAsync(character);
            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        /// <summary>
        /// Deletes a character by their Id.
        /// Returns a failed state if the character doesnt exist with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacterAsync(int id)
        {
            try
            {
                await _characterService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }

}
