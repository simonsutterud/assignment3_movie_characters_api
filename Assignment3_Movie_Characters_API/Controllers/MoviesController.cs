﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Models.DTO.Character;
using Assignment3_Movie_Characters_API.Models.DTO.Movie;
using Assignment3_Movie_Characters_API.Services.Character;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Assignment3_Movie_Movies_API.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Gets all the movies in the database with their related franchises represented as Ids.
        /// </summary>
        /// <returns>List of MovieGetDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieGetDTO>>> GetMovies()
        {
            return Ok(
                _mapper.Map<List<MovieGetDTO>>(
                    await _movieService.GetAllAsync())
                );
        }

        /// <summary>
        /// Gets a single movie with the specified Id and maps to MovieGetDTO
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Single MovieGetDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieGetDTO>> GetMovie(int id)
        {
            try
            {
                return Ok(_mapper.Map<MovieGetDTO>(
                        await _movieService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates an existing movie. (Just values specific to the movie entity.
        /// Other, related data can be updated in separate endpoints.)
        /// Returns a failed state if the Ids dont match or the movie does not exist.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieAsync(int id, MoviePutDTO movie)
        {
            if (id != movie.Id)
                return BadRequest();

            try
            {
                await _movieService.UpdateAsync(
                       _mapper.Map<Movie>(movie)
                   );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Adds a new movie to the database.
        /// </summary>
        /// <param name="movieDto"></param>
        [HttpPost]
        public async Task<IActionResult> PostMovieAsync(MoviePostDTO movieDto)
        {
            Movie movie = _mapper.Map<Movie>(movieDto);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Deletes a movie by its Id.
        /// Returns a failed state if the movie doesnt exist with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovieAsync(int id)
        {
            try
            {
                await _movieService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the characters in a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of characters in a CharacterGetDTO format</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharactersForMovieAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterGetDTO>>(
                            await _movieService.GetCharactersAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }


        /// <summary>
        /// Updates the characters in a movie. 
        /// Returns an error if movie doesnt exist.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="id"></param>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersForMovieAsync(int[] characterIds, int id)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(characterIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }


    }

}
