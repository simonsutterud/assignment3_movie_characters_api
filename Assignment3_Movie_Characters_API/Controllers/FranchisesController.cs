﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Models.DTO.Character;
using Assignment3_Movie_Characters_API.Models.DTO.Franchise;
using Assignment3_Movie_Characters_API.Models.DTO.Movie;
using Assignment3_Movie_Characters_API.Services.Character;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Assignment3_Movie_Franchises_API.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Gets all the franchises in the database with their related movies represented as Ids.
        /// </summary>
        /// <returns>List of FranchiseGetDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseGetDTO>>> GetFranchises()
        {
            return Ok(
                _mapper.Map<List<FranchiseGetDTO>>(
                    await _franchiseService.GetAllAsync())
                );
        }

        /// <summary>
        /// Gets a single franchise with the specified Id and maps to FranchiseGetDTO
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Single FranchiseGetDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseGetDTO>> GetFranchise(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseGetDTO>(
                        await _franchiseService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates an existing franchise. (Just values specific to the franchise entity.
        /// Other, related data can be updated in separate endpoints.)
        /// Returns a failed state if the Ids dont match or the franchise does not exist.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, FranchisePutDTO franchise)
        {
            if (id != franchise.Id)
                return BadRequest();

            try
            {
                await _franchiseService.UpdateAsync(
                       _mapper.Map<Franchise>(franchise)
                   );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Adds a new franchise to the database.
        /// </summary>
        /// <param name="franchiseDto"></param>
        [HttpPost]
        public async Task<IActionResult> PostFranchiseAsync(FranchisePostDTO franchiseDto)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Deletes a franchise by its Id.
        /// Returns a failed state if the franchise doesnt exist with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchiseAsync(int id)
        {
            try
            {
                await _franchiseService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the movies in a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of movies in a MoiveGetDTO format</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieGetDTO>>> GetMoviesForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieGetDTO>>(
                            await _franchiseService.GetMoviesAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the characters in a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of characters in a CharacterGetDTO format</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharactersForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterGetDTO>>(
                            await _franchiseService.GetCharactersAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }


        /// <summary>
        /// Updates the movies in a franchise. 
        /// Returns an error if franchise doesnt exist.
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchise(int[] movieIds, int id)
        {
            try
            {
                await _franchiseService.UpdateMoviesAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

    }

}
