﻿namespace Assignment3_Movie_Characters_API.Services.Characters

{
    public interface ICharacterService : ICrudService<Models.Domain.Character, int>
    {
    }
}
