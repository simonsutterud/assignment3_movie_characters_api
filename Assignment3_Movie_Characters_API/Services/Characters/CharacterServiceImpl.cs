﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models;
using Assignment3_Movie_Characters_API.Services.Characters;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Movie_Characters_API.Services.Character
{
    public class CharacterServiceImpl : ICharacterService
    {
        private readonly MovieDbContext _context;
        public CharacterServiceImpl(MovieDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Models.Domain.Character entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            // Log and throw pattern
            if (character == null)
            {
                throw new EntityNotFoundException("No character found with that ID!");
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Models.Domain.Character>> GetAllAsync()
        {
            return await _context.Characters
               .Include(c => c.Movies)
               .ToListAsync();
        }

        public async Task<Models.Domain.Character> GetByIdAsync(int id)
        {
            if (!await CharacterExistsAsync(id))
            {
                throw new EntityNotFoundException("No character found with that ID!");
            }
            // Want to include all related data
            return await _context.Characters
                .Where(c => c.Id == id)
                .Include(c => c.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Models.Domain.Character entity)
        {
            // Log and throw pattern
            if (!await CharacterExistsAsync(entity.Id))
            {
                throw new EntityNotFoundException("No character found with that ID!");
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a professor exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If professor exists</returns>
        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _context.Characters.AnyAsync(e => e.Id == id);
        }

    }
}
