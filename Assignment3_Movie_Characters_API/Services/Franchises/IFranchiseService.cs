﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models.Domain;

namespace Assignment3_Movie_Characters_API.Services.Character
{
    public interface IFranchiseService : ICrudService<Models.Domain.Franchise, int>
    {
        /// <summary>
        /// Gets all the movies in a franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A collection of movies</returns>
        /// <exception cref="EntityNotFoundException">Thrown when a franchise does not exist with the specified Id in the database.</exception>
        Task<ICollection<Movie>> GetMoviesAsync(int franchiseId);

        /// <summary>
        /// Gets all the characters in a franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A collection of characters</returns>
        /// <exception cref="EntityNotFoundException">Thrown when a franchise does not exist with the specified Id in the database.</exception>
        Task<ICollection<Models.Domain.Character>> GetCharactersAsync(int franchiseId);

        /// <summary>
        /// Updates the movies in a franchise. This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="franchiseId"></param>
        /// <exception cref="EntityNotFoundException">Thrown when a franchise does not exist with the specified Id in the database.</exception>
        Task UpdateMoviesAsync(int[] movieIds, int franchiseId);
    }
}
