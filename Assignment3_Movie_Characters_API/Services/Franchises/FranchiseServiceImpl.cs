﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models;
using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Services.Character;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Movie_Franchises_API.Services.Franchise
{
    public class FranchiseServiceImpl : IFranchiseService
    {
        private readonly MovieDbContext _context;
        public FranchiseServiceImpl(MovieDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Assignment3_Movie_Characters_API.Models.Domain.Franchise entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            // Log and throw pattern
            if (franchise == null)
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Assignment3_Movie_Characters_API.Models.Domain.Franchise>> GetAllAsync()
        {
            var franchises = await _context.Franchises
               .Include(f => f.Movies)
               .ToListAsync();
            return franchises;
        }

        public async Task<Assignment3_Movie_Characters_API.Models.Domain.Franchise> GetByIdAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }
            // Want to include all related data
            return await _context.Franchises
                .Where(c => c.Id == id)
                .Include(c => c.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Character>> GetCharactersAsync(int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }

            var chars = await _context.Movies
                .Where(m => m.FranchiseId == franchiseId)
                .Select(m => m.Characters)
                .FirstOrDefaultAsync();

            return chars;
        }

        public async Task<ICollection<Movie>> GetMoviesAsync(int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }

            var movies = await _context.Movies
                .Where(m => m.FranchiseId == franchiseId)
                .ToListAsync();

            return movies;
        }

        public async Task UpdateAsync(Assignment3_Movie_Characters_API.Models.Domain.Franchise entity)
        {
            // Log and throw pattern
            if (!await FranchiseExistsAsync(entity.Id))
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                throw new EntityNotFoundException("No franchise found with that ID!");
            }
            // First convert the int[] to List<Student>

            // Could utilize the StudentNotFoundException here
            // if there is an Id in the array that doesnt have a student.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _context.Movies
                .Where(m => m.Id == mid).First())
                .ToList();

            // Get professor for Id
            Assignment3_Movie_Characters_API.Models.Domain.Franchise franchise = await _context.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();
            // Set the professors students
            franchise.Movies = movies;
            _context.Entry(franchise).State = EntityState.Modified;
            // Save all the changes
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a franchise exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If professor exists</returns>
        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _context.Franchises.AnyAsync(e => e.Id == id);
        }
    }
}
