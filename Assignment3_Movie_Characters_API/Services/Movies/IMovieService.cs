﻿using Assignment3_Movie_Characters_API.Exceptions;

namespace Assignment3_Movie_Characters_API.Services.Character
{
    public interface IMovieService : ICrudService<Models.Domain.Movie, int>
    {

        /// <summary>
        /// Gets all the characters in a movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>A collection of characters</returns>
        /// <exception cref="EntityNotFoundException">Thrown when a movie does not exist with the specified Id in the database.</exception>
        Task<ICollection<Models.Domain.Character>> GetCharactersAsync(int movieId);


        /// <summary>
        /// Updates the characters in a movie. This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="movieId"></param>
        /// <exception cref="EntityNotFoundException">Thrown when a movie does not exist with the specified Id in the database.</exception>
        Task UpdateCharactersAsync(int[] characterIds, int movieId);

    }
}
