﻿using Assignment3_Movie_Characters_API.Exceptions;
using Assignment3_Movie_Characters_API.Models;
using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Services.Character;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Movie_Movies_API.Services.Movie
{
    public class MovieServiceImpl : IMovieService
    {
        private readonly MovieDbContext _context;
        public MovieServiceImpl(MovieDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Assignment3_Movie_Characters_API.Models.Domain.Movie entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            // Log and throw pattern
            if (movie == null)
            {
                throw new EntityNotFoundException("No movie found with that ID!");
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Assignment3_Movie_Characters_API.Models.Domain.Movie>> GetAllAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<Assignment3_Movie_Characters_API.Models.Domain.Movie> GetByIdAsync(int id)
        {
            if (!await MovieExistsAsync(id))
            {
                throw new EntityNotFoundException("No movie found with that ID!");
            }
            // Want to include all related data
            return await _context.Movies.FirstAsync();
        }

        public async Task<ICollection<Character>> GetCharactersAsync(int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                throw new EntityNotFoundException("No movie found with that ID!");
            }

            var chars = await _context.Movies
                .Where(m => m.Id == movieId)
                .Select(m => m.Characters)
                .FirstOrDefaultAsync();

            return chars;
        }

        public async Task UpdateAsync(Assignment3_Movie_Characters_API.Models.Domain.Movie entity)
        {
            // Log and throw pattern
            if (!await MovieExistsAsync(entity.Id))
            {
                throw new EntityNotFoundException("No movie found with that ID!");
            }
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            // Log and throw pattern
            if (!await MovieExistsAsync(movieId))
            {
                throw new EntityNotFoundException("No movie found with that ID!");
            }
            // First convert the int[] to List<Student>

            // Could utilize the StudentNotFoundException here
            // if there is an Id in the array that doesnt have a student.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Character> characters = characterIds
                .ToList()
                .Select(cid => _context.Characters
                .Where(c => c.Id == cid).First())
                .ToList();
            // Get professor for Id
            Assignment3_Movie_Characters_API.Models.Domain.Movie movie = await _context.Movies
                .Where(m => m.Id == movieId)
                .FirstAsync();
            // Set the professors students
            movie.Characters = characters;
            _context.Entry(movie).State = EntityState.Modified;
            // Save all the changes
            await _context.SaveChangesAsync();

        }

        /// <summary>
        /// Simple utility method to help check if a professor exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If professor exists</returns>
        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _context.Movies.AnyAsync(e => e.Id == id);
        }
    }
}
