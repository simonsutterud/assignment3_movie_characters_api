﻿using Assignment3_Movie_Characters_API.Models.Domain;
using AutoMapper;

namespace Assignment3_Movie_Characters_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, Models.DTO.Character.CharacterGetDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToList()));

            CreateMap<Models.DTO.Character.CharacterPostDTO, Character>();

            CreateMap<Models.DTO.Character.CharacterPutDTO, Character>();
        }

    }
}
