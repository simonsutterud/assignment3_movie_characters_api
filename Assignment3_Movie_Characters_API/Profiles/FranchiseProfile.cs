﻿using Assignment3_Movie_Characters_API.Models.Domain;
using Assignment3_Movie_Characters_API.Models.DTO.Franchise;
using AutoMapper;

namespace Assignment3_Movie_Franchises_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseGetDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToList()));

            CreateMap<FranchisePostDTO, Franchise>();

            CreateMap<FranchisePutDTO, Franchise>();
        }
    }
}
