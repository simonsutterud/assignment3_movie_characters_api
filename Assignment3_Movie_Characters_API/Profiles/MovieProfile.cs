﻿using Assignment3_Movie_Characters_API.Models.Domain;
using AutoMapper;

namespace Assignment3_Movie_Characters_API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, Models.DTO.Movie.MovieGetDTO>();

            CreateMap<Models.DTO.Movie.MoviePostDTO, Movie>();

            CreateMap<Models.DTO.Movie.MoviePutDTO, Movie>();
        }
    }
}
